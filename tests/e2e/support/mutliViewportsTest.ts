import {TViewportDef} from "./viewports";

/// <reference types="cypress" />

function testMultipleViewports(name: string, viewports: TViewportDef[], test: (viewport: TViewportDef) => void) {
  for (const vp of viewports) {
    it(name.replace("[vp]", `[${vp[0]}x${vp[1]}]`), () => {
      cy.viewport(vp[0], vp[1]);

      test.call(context, vp);
    });
  }
}

export default testMultipleViewports;
