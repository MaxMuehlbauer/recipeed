export type TViewportDef = [number, number];
export type TAvailableViewports = "xs" | "sm" | "md" | "lg" | "xl";
export type TViewportsDef = {
  [key in TAvailableViewports]: TViewportDef
};

const viewports: TViewportsDef = {
  xs: [380, 676],
  sm: [620, 1100],
  md: [992, 780],
  lg: [1200, 675],
  xl: [1920, 1080]
};

export const xs = viewports.xs;
export const sm = viewports.sm;
export const md = viewports.md;
export const lg = viewports.lg;
export const xl = viewports.xl;
