/// <reference types="cypress" />

import {xs, sm, md, lg, xl} from "../support/viewports";
import testMultipleViewports from "../support/mutliViewportsTest";

describe("Navigation", () => {
  context("expandable navigation", () => {
    const viewports = [xs, sm, md];

    testMultipleViewports("should be closed on page load [vp]", viewports, () => {
      cy.visit("/")
          .wait(200)
          .get("[data-cy=navigation]")
          .then(($el) => {
            const offset = $el.offset();
            if (offset === undefined) {
              expect(true).equals(false);
            } else {
              expect(offset.left).to.be.lt(0);
            }
          });
    });

    testMultipleViewports("should open on click on trigger [vp]", viewports, () => {
      cy.visit("/")
          .wait(200)
          .get("[data-cy=navigation-trigger]")
          .click()
          .wait(300)
          .get("[data-cy=navigation]")
          .then(($el) => {
            const offset = $el.offset();
            if (offset === undefined) {
              expect(true).equals(false);
            } else {
              expect(offset.left).equals(0);
            }
          });
    });
  });
});
