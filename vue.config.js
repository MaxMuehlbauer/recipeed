const path = require('path');

module.exports = {
  baseUrl: '',

  css: {
    sourceMap: true
  },

  pwa: {
    name: 'recipeed',
    themeColor: '#2af1a1',
    msTileColor: '#2af1a1',
    workboxOptions: {
      importWorkboxFrom: 'local'
    }
  },

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [
        path.resolve(__dirname, 'src/styles/config.scss')
      ]
    }
  }
};
