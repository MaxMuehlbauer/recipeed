import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import NewRecipe from "./views/NewRecipe.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/create/",
      name: "create",
      component: NewRecipe
    }
  ],
});
