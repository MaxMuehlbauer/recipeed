import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

// @ts-ignore
import Vue2TouchEvents from "vue2-touch-events";

Vue.config.devtools = true;
Vue.config.productionTip = false;

Vue.use(VueAxios, axios);
Vue.use(Vue2TouchEvents);


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
